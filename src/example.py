from cog import Cog, ConfigOption, SlashOption

class Example(Cog):

    # Built-in attributes:
    # self.config - A dictionary containing the configuration values of the cog
    # self.db - The Database object of the cog, for persistent data
    # self.directory - The Path of the folder containing this cog
    # self.metadata - A dictionary containing meta data about the cog, from pyproject.toml

    class Config:
        is_true: bool = ConfigOption(default=True, description="Whether or not this value is true")

    def load(self):
        print('Loaded!')

    async def ready(self):
        print('Async Loaded!')
    
    def unload(self):
        print('Unloaded!')

    @Cog.command()
    async def ping(self, ctx: Interaction):
        await ctx.send("Pong!")

    @Cog.listener()
    async def on_member_join(self, member):
        print(f"{member.mention} has joined {member.guild.name}")